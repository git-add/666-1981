# 666-1981

`2 books` `all files encrypted`

---

[Geometry](./bok%253A978-1-4684-0130-1.zip)
<br>
A Metric Approach with Models
<br>
Richard S. Millman, George D. Parker in Undergraduate Texts in Mathematics (1981)

---

[Optimization Techniques](./bok%253A978-1-4613-9458-7.zip)
<br>
An Introduction
<br>
L. R. Foulds in Undergraduate Texts in Mathematics (1981)

---
